import UIKit

// ①・・・RealmSwiftをimport
import RealmSwift


class ListView: UIViewController , UITextFieldDelegate {


  //テキストフィールドとテーブルビューを紐付け

    @IBOutlet weak var テーブル: UITableView!
    @IBOutlet weak var テキストフィールド: UITextField!
    
    // ②・・・作成したTodoModel型の変数を用意。<TodoModel>という書き方はいわゆるジェネリック
  //Realmから受け取るデータを突っ込む変数を準備
  var アイテムリスト: Results<TodoModel>!



  override func viewDidLoad() {
    super.viewDidLoad()

    // ③・・・Realmをインスタンス化
    // Realmのインスタンスを取得
    let Realmインスタンス1 = try! Realm()

    // ④・・・Realmのfunctionでデータを取得。functionを更に追加することで、フィルターもかけられる
    // Realmデータベースに登録されているデータを全て取得
    // try!はエラーが発生しなかった場合は通常の値が返されるが、エラーの場合はクラッシュ
    self.アイテムリスト = Realmインスタンス1.objects(TodoModel.self)
  }


  // 追加ボタンを押したら発動


    @IBAction func 追加ボタン(_ sender: Any) {
    
    // モデルクラスをインスタンス化
    let インスタンス化したTodoModel:TodoModel = TodoModel()

    // テキストフィールドの名前を突っ込む
    インスタンス化したTodoModel.項目名 = self.テキストフィールド.text

    // Realmデータベースを取得
    // try!はエラーが発生しなかった場合は通常の値が返されるが、エラーの場合はクラッシュ
    let Realmインスタンス2 = try! Realm()

    // ⑤・・・Realmインスタンスからaddを叩くと、データベースにレコードが追加される
    // テキストフィールドの情報をデータベースに追加
    try! Realmインスタンス2.write {
      Realmインスタンス2.add(インスタンス化したTodoModel)
    }

    // テーブルリストを再読み込み
    self.テーブル.reloadData()
  }
}

extension ListView: UITableViewDataSource {

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.アイテムリスト.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

    // TodoModelクラス型の変数を宣言
    let セル:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "テストセル", for: indexPath)

    // 取得したTodoリストからn番目を変数に代入
    let アイテム: TodoModel = self.アイテムリスト[(indexPath as NSIndexPath).row];

    // 取得した情報をセルに反映
    セル.textLabel?.text = アイテム.項目名

    return セル
  }
}
