
import UIKit

class ImageView: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func pictureSetting(_ sender: Any) {
        let ipc = UIImagePickerController()
            ipc.delegate = self
            ipc.sourceType = UIImagePickerController.SourceType.photoLibrary
     
            ipc.allowsEditing = false
            self.present(ipc,animated: false, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if info[UIImagePickerController.InfoKey.originalImage] != nil {
            let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        imageView.image = image
        }
        dismiss(animated: true, completion: nil)
    }
}
