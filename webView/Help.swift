
import UIKit
 
class Help: UIViewController,
UITableViewDataSource, UITableViewDelegate{

@IBOutlet weak var table: UITableView!
    
    //データ設定
    let imgArray: NSArray =
        ["img0","img1","img2","img3","img4","img5","img6"]
    
    let label2Array: NSArray = ["ブラウザを開く","ブックマークを開く","履歴を開く","ホームの画像を変更する","前のページに戻る","先のページに進む","タブを開く"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //Table Viewのセルの数を指定
    func tableView(_ table: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return imgArray.count
    }
    
    //各セルの要素を設定する
    func tableView(_ table: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //UITableViewCell のインスタンスを生成
        let cell = table.dequeueReusableCell(withIdentifier: "tableCell",
                                             for: indexPath)
        
        let img = UIImage(named: imgArray[indexPath.row] as! String)
        
        //UIImageView インスタンスの生成
        let imageView = cell.viewWithTag(1) as! UIImageView
        imageView.image = img
        
        //UILabel インスタンスの生成
        let label2 = cell.viewWithTag(2) as! UILabel
        label2.text = String(describing: label2Array[indexPath.row])
        
        return cell
    }
}
