import UIKit
import WebKit

let screenSize: CGSize = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, WKUIDelegate, WKNavigationDelegate {

    var itemCount = 1
    var selectedIndex: IndexPath = [0,0]
    var selectedNum = 0
    var currentUrl = "https://www.google.com"

    var webView: WKWebView!

    var webTitle: String = "Google"
    var urlArray:[String] = ["https://www.google.com"]
    var webTitleArray: [String] = ["Google"]

    let collectionView: UICollectionView = {
        //セルのレイアウト設計
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 100, width: screenSize.width, height: 50), collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.white
        //セルの登録
        collectionView.register(CollectionViewCell.self, forCellWithReuseIdentifier: "CollectionViewCell")
        return collectionView
    }()


    //追加ボタンの設定
    func buttonUpDate(){
        let addTabButton:UIButton = UIButton(frame: CGRect(x: 0, y: 50, width: 50, height: 50))
        addTabButton.backgroundColor = .white
        addTabButton.setTitle("+", for: .normal)
        addTabButton.setTitleColor(UIColor(red: 22/225, green: 157/225, blue: 202/225, alpha:0.5), for: .normal)
        addTabButton.addTarget(self, action: #selector(pushAddTabButton), for: .touchUpInside)
        self.view.addSubview(addTabButton)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        view.addSubview(collectionView)
        newWeb()
        webView.uiDelegate = self
        webView.navigationDelegate = self
        buttonUpDate()
        closeButtonUpDate()
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {

        if webView.title != nil {
            let webText = webView.title as! String
            print("\(webText)")
            webTitle = "\(webText)"
            webTitleArray[selectedNum] = webTitle
            collectionView.reloadData()
        } else {
            webTitle = webView.url!.absoluteString
            webTitleArray[selectedNum] = webTitle
            collectionView.reloadData()
        }

    }

    //削除ボタンの定義
    func closeButtonUpDate(){
        let tabCloseButton:UIButton = UIButton(frame: CGRect(x: 50, y: 50, width: 150, height: 50))
        tabCloseButton.backgroundColor = .white
        tabCloseButton.setTitle("現在のタブを削除", for: .normal)
        tabCloseButton.setTitleColor(UIColor(red: 22/225, green: 157/225, blue: 202/225, alpha:0.5), for: .normal)
        tabCloseButton.addTarget(self, action: #selector(tapCloseButton), for: .touchUpInside)
        self.view.addSubview(tabCloseButton)
    }
    //cellの個数設定
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemCount
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell

        let title = webTitleArray[indexPath.item]
        cell.setupContents(textName: title)

        return cell
    }

    //新規タブの処理
    @objc func pushAddTabButton(sender: UIButton){
        itemCount += 1//add tab
        collectionView.reloadData()//add tab
        currentUrl = self.webView.url!.absoluteString
        urlArray[selectedNum] = currentUrl
        if webView != nil {
            webView.removeFromSuperview()//delete old web
        }
        newWeb()//add new google
        urlArray += ["https://www.google.com"]
        webTitleArray += ["Google"]
        selectedNum = itemCount - 1
        selectedIndex[1] = itemCount - 1
    }

    //タブタップ時の処理
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        currentUrl = self.webView.url!.absoluteString
        urlArray[selectedNum] = currentUrl
        webTitleArray[selectedNum] = webTitle
        selectedIndex = indexPath
        selectedNum = indexPath[1]
        upDateWeb(index: selectedNum)
        collectionView.reloadData()
    }

    //タブ削除の処理
    @objc func tapCloseButton(sender: UIButton){

        if itemCount == 1 && selectedNum == 0 {
            //ラスト1つだけのタブを削除した時→リロード。googleの画面が0番目のタブにセットされる
            if webView != nil {
                webView.removeFromSuperview()//delete old web
            }
            urlArray = ["https://www.google.com"]//urlArrayは初期化
            webTitleArray = ["Google"]
            newWeb()
        } else if selectedNum == 0 && itemCount != 1 {
            //0番目のタブを削除した時→1番目のタブに飛ぶ
            if webView != nil {
                webView.removeFromSuperview()//delete old web
            }
            itemCount -= 1
            collectionView.deleteItems(at: [selectedIndex])
            collectionView.reloadData()
            upDateWeb(index: 1)
            urlArray.remove(at: 0)
            webTitleArray.remove(at: 0)
        } else {
            itemCount -= 1
            collectionView.deleteItems(at: [selectedIndex])
            collectionView.reloadData()
            urlArray.remove(at: selectedNum)//selectされたIndexに対応するURLを削除
            webTitleArray.remove(at: selectedNum)
            //一つ若いタブに格納されたurlでwebview
                    selectedNum = selectedNum - 1
                    selectedIndex = [0, selectedNum - 1]
                    upDateWeb(index: selectedNum)
                }

            }

            func newWeb(){
                webView = WKWebView(frame: CGRect(x: 0, y: 150, width: screenSize.width, height: screenSize.height))
                let urlString = "https://www.google.com"
                let encodedUrlString = urlString.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
                let url = NSURL(string: encodedUrlString!)
                let request = NSURLRequest(url: url! as URL)
                webView.load(request as URLRequest)
                self.view.bringSubviewToFront(webView)
                self.view.addSubview(webView)
                webView.uiDelegate = self
                webView.navigationDelegate = self
            }

            func upDateWeb(index: Int){
                if webView != nil {
                webView.removeFromSuperview()
            }
            print(urlArray)
            print(index)
            webView = WKWebView(frame: CGRect(x: 0, y: 150, width: screenSize.width, height: screenSize.height))
            let urlString = urlArray[index]
            let encodedUrlString = urlString.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
            let url = NSURL(string: encodedUrlString!)
            let request = NSURLRequest(url: url! as URL)
            webView.load(request as URLRequest)
            self.view.bringSubviewToFront(webView)
            self.view.addSubview(webView)
            webView.uiDelegate = self
            webView.navigationDelegate = self
        }

    }

    //cellのサイズの設定
    extension ViewController: UICollectionViewDelegateFlowLayout {

        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: screenSize.width * 2 / 5, height: 50)
        }
    }

