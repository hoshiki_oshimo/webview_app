
import UIKit
import RealmSwift

class BookmarkList: UIViewController {
  private var realm: Realm!
  private var bookmarkItem: Results<BookmarkItem>!
  private var token: NotificationToken!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func awakeFromNib() {
    super.awakeFromNib()

    // RealmのTodoリストを取得、更新を管理
        realm = try! Realm()
        bookmarkItem = realm.objects(BookmarkItem.self)
        token = bookmarkItem.observe { [weak self] _ in self?.reload()}
    }

    deinit {
        token.invalidate()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    @IBAction func addTapped(_ sender: Any) {
    // 新規ブックマーク追加用のダイアログを表示
        let dlg = UIAlertController(title: "URLを追加", message: "", preferredStyle: .alert)
        dlg.addTextField(configurationHandler: nil)
        dlg.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            if let t = dlg.textFields![0].text,!t.isEmpty {
                self.addTodoItem(title: t)
            }
        }))
    present(dlg, animated: true)
  }

  //追加
    func addTodoItem(title: String) {
        try! realm.write {realm.add(BookmarkItem(value: ["title": title]))}
        
    }

  //削除
    func deleteTodoItem(at index: Int) {
        try! realm.write {realm.delete(bookmarkItem[index])}
    }

    func reload() {
        tableView.reloadData()
    }
    
}


extension BookmarkList: UITableViewDelegate {
}

extension BookmarkList: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return bookmarkItem.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "todoItem", for: indexPath)
    cell.textLabel?.text = bookmarkItem[indexPath.row].title
    return cell
  }

  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }

  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    deleteTodoItem(at: indexPath.row)
  }
    
}
