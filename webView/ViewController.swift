
import UIKit
import WebKit
import RealmSwift

class ViewController: UIViewController, WKUIDelegate,WKNavigationDelegate,UISearchBarDelegate {
    
    var favoriteList:[String] = []
    var saveUd = UserDefaults.standard
    var searchBar: UISearchBar!
    
    @IBOutlet weak var webView: WKWebView!
    
    @IBAction func backAction(_ sender: Any) {
        webView.goBack()
    }
    
    @IBAction func goAction(_ sender: Any) {
        webView.goForward()
    }
    
    
    override func viewDidLoad() {
        setupSearchBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let webUrl = URL(string:"https://www.google.com")!
        let myRequest = URLRequest(url: webUrl)
        webView.load(myRequest)
        self.view.addSubview(webView)
    }
    
    func setupSearchBar() {
        if let navigationBarFrame = navigationController?.navigationBar.bounds {
            let searchBar: UISearchBar = UISearchBar(frame: navigationBarFrame)
            searchBar.delegate = self
            searchBar.placeholder = "タイトルで探す"
            searchBar.tintColor = UIColor.gray
            searchBar.keyboardType = UIKeyboardType.default
            navigationItem.titleView = searchBar
            navigationItem.titleView?.frame = searchBar.frame
            self.searchBar = searchBar
        }
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.showsCancelButton = true
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
}
