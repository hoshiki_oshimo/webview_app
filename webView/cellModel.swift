//
//  cellModel.swift
//  webView
//
//  Created by hoshimo on 2020/05/13.
//  Copyright © 2020 hoshimo. All rights reserved.
//

import Foundation

class Model : NSObject {
    var name:NSString
    var imageUrl:NSURL?
    
    init(name: String, imageUrl: NSURL?){
        self.name = name
        self.imageUrl = imageUrl
    }
}
