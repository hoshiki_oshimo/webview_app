import UIKit

class CollectionViewCell: UICollectionViewCell {

    let tabCellLabel: UILabel = {
        let tabCellLabel = UILabel()
        tabCellLabel.frame = CGRect(x: 0, y: 0, width: screenSize.width * 2 / 5, height: 50)
        tabCellLabel.textColor = UIColor(red: 225/225, green: 112/225, blue: 0/225, alpha:1.0)
        tabCellLabel.textAlignment = .center
        return tabCellLabel
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    func setup() {
        layer.borderColor = UIColor.lightGray.cgColor
        layer.backgroundColor = UIColor(red: 22/225, green: 157/225, blue: 202/225, alpha:0.5).cgColor
        layer.borderWidth = 3.0
        contentView.addSubview(tabCellLabel)
        contentView.bringSubviewToFront(tabCellLabel)
    }

    func setupContents(textName: String) {
        tabCellLabel.text = textName
    }
}
